﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumeraablidExtensionidLinq
{
    static class Program
    {
        static void Main(string[] args)
        {

            List<int> arvud = new List<int> { 1, 5, 2, 8, 6, 3, 1, 7 };

            foreach (var x in arvud.Where(x => x % 3 == 0)) Console.Write($"{x.Ruut()} ");
            Console.WriteLine();
        }

        static int Ruut(this int x) => x * x;
 


      



    }
}
